﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Events.Core.Models;
using FluentAssertions;

namespace Events.Tests.Models
{
    [TestClass]
    public class AttendanceTest
    {
        [TestMethod]
        public void NumOfParticipants_DeclareEmptyAttendance_ShouldSetNumOfParticiapantsToOne()
        {
            var result = new Attendance();

            result.NumOfParticipants.ShouldBeEquivalentTo("1");
        }
    }
}
