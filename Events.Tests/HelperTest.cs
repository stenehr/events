﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Events.Core;
using FluentAssertions;

namespace Events.Tests
{
    [TestClass]
    public class HelperTest
    {
        [TestMethod]
        public void GetFirstName_EmptyOrWhiteSpaceOrNullFullName_ShouldReturnNull()
        {
            var helper = new Helper();

            var result = helper.GetFirstName("");
            var result2 = helper.GetFirstName("   ");
            var result3 = helper.GetFirstName(null);

            result.ShouldBeEquivalentTo(null);
            result.ShouldBeEquivalentTo(null);
            result3.ShouldBeEquivalentTo(null);
        }

        [TestMethod]
        public void GetFirstName_OnlySingleFirstAndLastName_ShouldReturnSingleFirstName()
        {
            var fullName = "Kersti Kaljulaid";
            var helper = new Helper();

            var result = helper.GetFirstName(fullName);

            result.ShouldBeEquivalentTo("Kersti");
        }

        [TestMethod]
        public void GetFirstName_ThreeWordName_ShouldReturnFirstTwoAsFirstName()
        {
            var fullName = "AA BB CC";
            var helper = new Helper();

            var result = helper.GetFirstName(fullName);

            result.ShouldBeEquivalentTo("AA BB");
        }

        [TestMethod]
        public void GetFirstName_FourWordName_ShouldReturnFirstThreeAsFirstName()
        {
            var fullName = "AA BB CC DD";
            var helper = new Helper();

            var result = helper.GetFirstName(fullName);

            result.ShouldBeEquivalentTo("AA BB CC");
        }

        [TestMethod]
        public void GetLastName_EmptyOrWhiteSpaceOrNullFirstName_ShouldReturnNull()
        {
            var helper = new Helper();

            var result = helper.GetLastName("");
            var result2 = helper.GetLastName("   ");
            var result3 = helper.GetLastName(null);

            result.ShouldBeEquivalentTo(null);
            result2.ShouldBeEquivalentTo(null);
            result3.ShouldBeEquivalentTo(null);
        }

        [TestMethod]
        public void GetLastName_OnlySingleFirstAndLastName_ShouldReturnLastName()
        {
            var fullname = "Kersti Kaljulaid";
            var helper = new Helper();

            var result = helper.GetLastName(fullname);

            result.ShouldBeEquivalentTo("Kaljulaid");
        }

        [TestMethod]
        public void GetLastName_FourWordName_ShouldReturnLastName()
        {
            var fullname = "AA BB CC DD";
            var helper = new Helper();

            var result = helper.GetLastName(fullname);

            result.ShouldBeEquivalentTo("DD");
        }

        [TestMethod]
        public void GetDateTime_OnIllegalInput_ShouldReturnDateTimeMinValue()
        {
            var date = "aa";
            var time = "bb";
            var helper = new Helper();

            var result = helper.GetDateTime(date, time);

            result.ShouldBeEquivalentTo(DateTime.MinValue);
        }

        [TestMethod]
        public void GetDateTime_OnUnexpectedFormat_ShouldReturnDateTimeMinValue()
        {
            var date = "11/27/2017";
            var time = "8:00:00 PM";
            var helper = new Helper();

            var result = helper.GetDateTime(date, time);

            result.ToString().ShouldBeEquivalentTo(DateTime.MinValue);
        }

        [TestMethod]
        public void GetDateTime_OnExpectedFormat_ShouldReturnCorrectDateTime()
        {
            var date = "27.11.2017";
            var time = "20.00";
            var helper = new Helper();

            var result = helper.GetDateTime(date, time);

            result.ToString().ShouldBeEquivalentTo("11/27/2017 8:00:00 PM");
        }
    }
}
