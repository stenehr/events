﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Events.Persistence.Repositories;
using Moq;
using System.Data.Entity;
using Events.Core.Models;
using Events.Persistence;
using Events.Tests.Extensions;
using FluentAssertions;
using System.Linq;

namespace Events.Tests.Repositories
{
    [TestClass]
    public class AttendanceRepositoryTest
    {
        private AttendaceRepository _repository;
        private Mock<DbSet<Attendance>> _mockAttendances;

        [TestInitialize]
        public void TestInitialize()
        {
            _mockAttendances = new Mock<DbSet<Attendance>>();
            var mockContext = new Mock<IAppDbContext>();

            mockContext.SetupGet(c => c.Attendances).Returns(_mockAttendances.Object);

            _repository = new AttendaceRepository(mockContext.Object);
        }

        [TestMethod]
        public void GetAttendancesByMeetingId_MeetingIdNotSet_ShouldNotReturn()
        {
            var attendance = new Attendance() { Id = 1 };

            _mockAttendances.SetSource(new List<Attendance> { attendance });

            var results = _repository.GetAttendanceByMeetingId(2);

            results.Should().BeEmpty();
        }

        [TestMethod]
        public void GetAttendancesByMeetingId_MeetingIdIsSet_ShouldReturn()
        {
            var attendance1 = new Attendance() { MeetingId = 1 };
            var attendance2 = new Attendance() { MeetingId = 1 };

            _mockAttendances.SetSource(new List<Attendance> { attendance1, attendance2 });

            var results = _repository.GetAttendanceByMeetingId(1);

            results.Should().NotBeEmpty();
            results.Count().ShouldBeEquivalentTo(2);
        }

        [TestMethod]
        public void GetAttendanceByMeetingIdAttendeeCode_AttendeeCodeIsFalse_ShouldReturnNull ()
        {
            var attendance = new Attendance() { MeetingId = 1, IdentityCode = "2" };

            _mockAttendances.SetSource(new List<Attendance> { attendance });

            var result = _repository.GetAttendanceByMeetingIdAttendeeCode(1, "1");

            result.ShouldBeEquivalentTo(null);
        }

        [TestMethod]
        public void GetAttendanceByMeetingIdAttendeeCode_MeetingIdAndAttendeeCodeIsCorrect_ShouldReturn()
        {
            var attendance = new Attendance() { MeetingId = 1, IdentityCode = "2", Name = "Test" };

            _mockAttendances.SetSource(new List<Attendance> { attendance });

            var result = _repository.GetAttendanceByMeetingIdAttendeeCode(1, "2");
            
            result.MeetingId.ShouldBeEquivalentTo(1);
            result.IdentityCode.ShouldBeEquivalentTo("2");
            result.Name.ShouldAllBeEquivalentTo("Test");
        }

        [TestMethod]
        public void GetAttendance_RepositoryIsEmpty_ShouldReturnNull()
        {
            _mockAttendances.SetSource(new List<Attendance>());

            var result = _repository.GetAttendance(1);

            result.ShouldBeEquivalentTo(null);
        }

        [TestMethod]
        public void GetAttendance_IdIsCorrect_ShouldReturn()
        {
            var attendance = new Attendance() { Id = 1, Name = "Test" };

            _mockAttendances.SetSource(new List<Attendance> { attendance });

            var result = _repository.GetAttendance(1);

            result.Id.ShouldBeEquivalentTo(1);
            result.Name.ShouldAllBeEquivalentTo("Test");
        }
    }
}
