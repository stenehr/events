﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Events.Persistence.Repositories;
using Moq;
using Events.Persistence;
using Events.Core.Models;
using System.Data.Entity;
using Events.Tests.Extensions;
using FluentAssertions;
using System.Linq;

namespace Events.Tests.Repositories
{

    [TestClass]
    public class MeetingRepositoryTest
    {
        private MeetingRepository _repository;
        private Mock<DbSet<Meeting>> _mockMeetings;

        [TestInitialize]
        public void TestInitialize()
        {
            _mockMeetings = new Mock<DbSet<Meeting>>();
            var mockContext = new Mock<IAppDbContext>();

            mockContext.SetupGet(c => c.Meetings).Returns(_mockMeetings.Object);

            _repository = new MeetingRepository(mockContext.Object);
        }

        [TestMethod]
        public void GetUpcomingMeetings_MeetingInPast_ShouldNotBeReturned()
        {
            var meeting = new Meeting() { Date = DateTime.Now.AddDays(-1) };

            _mockMeetings.SetSource(new List<Meeting>{ meeting });

            var meetings = _repository.GetupcomingMeetings();

            meetings.Should().BeEmpty();
        }

        [TestMethod]
        public void GetUpcomingMeetings_MeetingInFuture_ShouldReturn()
        {
            var meeting = new Meeting() { Date = DateTime.Now.AddSeconds(1) };
            var meeting2 = new Meeting() { Date = DateTime.Now.AddYears(3) };

            _mockMeetings.SetSource(new List<Meeting> { meeting, meeting2 });

            var result = _repository.GetupcomingMeetings();

            result.Should().NotBeEmpty();
            result.Count().ShouldBeEquivalentTo(2);
        }

        [TestMethod]
        public void GetPassedMeetings_MeetingInFuture_ShouldNotReturned()
        {
            var meeting = new Meeting() { Date = DateTime.Now.AddSeconds(1) };

            _mockMeetings.SetSource(new List<Meeting> { meeting });

            var results = _repository.GetPastMeetings();

            results.Should().BeEmpty();
        }

        [TestMethod]
        public void GetPassedMeetings_MeetingInPast_ShouldReturn()
        {
            var meeting = new Meeting() { Date = DateTime.Now.AddSeconds(-1) };
            var meeting2 = new Meeting() { Date = DateTime.Now.AddYears(-20) };

            _mockMeetings.SetSource(new List<Meeting> { meeting, meeting2 });

            var results = _repository.GetPastMeetings();

            results.Should().NotBeEmpty();
            results.Count().ShouldBeEquivalentTo(2);
        }

        [TestMethod]
        public void GetMeeting_MeetingRepositoryIsEmpty_ShouldReturnNull()
        {
            _mockMeetings.SetSource(new List<Meeting>());

            var result = _repository.GetMeeting(1);

            result.ShouldBeEquivalentTo(null);
        }

        [TestMethod]
        public void GetMeeting_MeetingWithCorrectId_ShouldReturn()
        {
            var meeting = new Meeting() { Id = 1, Name = "Test" };

            _mockMeetings.SetSource(new List<Meeting> { meeting });

            var result = _repository.GetMeeting(1);

            result.Id.ShouldBeEquivalentTo(1);
            result.Name.ShouldBeEquivalentTo("Test");
        }

        [TestMethod]
        public void GetAllMeetings_MeetingsListIsEmpty_ShouldNotReturn()
        {
            _mockMeetings.SetSource(new List<Meeting>());

            var results = _repository.GetAllMeetings();

            results.Should().BeEmpty();
        }

        [TestMethod]
        public void GetAllMeetings_MeetingsListHasEntry_ShouldReturn()
        {
            var meeting = new Meeting();
            var meeting2 = new Meeting();
            var meeting3 = new Meeting();

            _mockMeetings.SetSource(new List<Meeting> { meeting, meeting2, meeting3 });

            var results = _repository.GetAllMeetings();

            results.Should().NotBeEmpty();
            results.Count().ShouldBeEquivalentTo(3);
        }
    }
}
