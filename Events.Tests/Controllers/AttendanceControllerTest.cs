﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Events.Controllers;
using Events.Core.Repositories;
using Moq;
using Events.Core;
using FluentAssertions;
using System.Web.Mvc;
using Events.Core.Models;
using Events.Core.ViewModels;
using System.Net;

namespace Events.Tests.Controllers
{
    [TestClass]
    public class AttendanceControllerTest
    {
        private AttendanceController _controller;
        private Mock<IAttendaceRepository> _repository;

        [TestInitialize]
        public void TestInitialize()
        {
            _repository = new Mock<IAttendaceRepository>();
            var paymentsRepository = new Mock<IPaymentTypeRepository>();
            var mockUoW = new Mock<IUnitOfWork>();
            mockUoW.SetupGet(u => u.Attendances).Returns(_repository.Object);
            mockUoW.SetupGet(u => u.PaymentTypes).Returns(paymentsRepository.Object);

            _controller = new AttendanceController(mockUoW.Object);
        }

        [TestMethod]
        public void Add_AttendeeTypeIndividual_ShouldReturnViewResultToAddIndividualAttendance_View()
        {
            ViewResult result = _controller.Add(1, "Individual") as ViewResult;

            result.ViewName.ShouldBeEquivalentTo("AddIndividualAttendance");
        }

        [TestMethod]
        public void Add_AttendeeTypeBuisness_ShouldReturnViewResultToAddBuisnessAttendance_View()
        {
            ViewResult result = _controller.Add(1, "Buisness") as ViewResult;

            result.ViewName.ShouldBeEquivalentTo("AddBuisnessAttendance");
        }

        [TestMethod]
        public void AddIndividual_IfMeetingIdAndIdentityCodeExist_ShouldReturnViewResultToEditIndividualAttendance_View()
        {
            var vm = new IndividualAttendanceFormViewModel() { MeetingId = 1, IdentityCode = "1" };
            var attendance = new Attendance() { Id = 1, MeetingId = 1, IdentityCode = "1" };
            _repository.Setup(r => r.GetAttendanceByMeetingIdAttendeeCode(1, "1")).Returns(attendance);

            ViewResult result = _controller.AddIndividual(vm) as ViewResult;

            result.ViewName.ShouldBeEquivalentTo("EditIndividualAttendance");
        }

        [TestMethod]
        public void Edit_GetRequestGivenIdDoesNotExist_ShouldReturnHttpNotFound()
        {
            var result = _controller.Edit(1);

            result.Should().BeOfType<HttpNotFoundResult>();
        }

        [TestMethod]
        public void Edit_GetRequestValidIdAttendeeTypeIndividual_ShouldReturnViewResultToEditIndividualAttendance_View()
        {
            var attendance = new Attendance() { Id = 1, AttendeeType = "Individual" };
            _repository.Setup(r => r.GetAttendance(1)).Returns(attendance);

            var result = _controller.Edit(1) as ViewResult;

            result.ViewName.ShouldBeEquivalentTo("EditIndividualAttendance");
        }

        [TestMethod]
        public void Edit_GetRequestValidIdAttendeeTypeBuisness_ShouldReturnViewResultToEditBuisnessAttendance()
        {
            var attendance = new Attendance() { Id = 1, AttendeeType = "Buisness" };
            _repository.Setup(r => r.GetAttendance(1)).Returns(attendance);

            var result = _controller.Edit(1) as ViewResult;

            result.ViewName.ShouldBeEquivalentTo("EditBuisnessAttendance");
        }

        [TestMethod]
        public void Edit_GetRequestValidIdAttendeeTypeDoesNotExist_ShouldReturnHttpNotFound()
        {
            var attendance = new Attendance() { Id = 1, AttendeeType = "None" };
            _repository.Setup(r => r.GetAttendance(1)).Returns(attendance);

            var result = _controller.Edit(1);

            result.Should().BeOfType<HttpNotFoundResult>();
        }

        [TestMethod]
        public void Delete_GivenIdDoesNotExist_ShouldReturnHttpNotFound()
        {
            var result = _controller.Delete(1);

            result.Should().BeOfType<HttpNotFoundResult>();
        }

        [TestMethod]
        public void Delete_GivenIdIsValid_ShouldReturnHttpStatusCodeOk()
        {
            var attendance = new Attendance() { Id = 1 };
            _repository.Setup(r => r.GetAttendance(1)).Returns(attendance);

            var result = _controller.Delete(1) as HttpStatusCodeResult;

            result.StatusCode.ShouldBeEquivalentTo((int)HttpStatusCode.OK);
        }
    }
}
