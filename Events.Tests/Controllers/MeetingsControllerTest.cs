﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Events.Controllers;
using Events.Core;
using Moq;
using Events.Core.Repositories;
using FluentAssertions;
using System.Web.Mvc;
using Events.Core.ViewModels;
using Events.Core.Models;
using System.Net;

namespace Events.Tests.Controllers
{

    [TestClass]
    public class MeetingsControllerTest
    {
        private MeetingsController _controller;
        private Mock<IMeetingRepository> _repository;

        [TestInitialize]
        public void TestInitialize()
        {
            _repository = new Mock<IMeetingRepository>();
            var mockUoW = new Mock<IUnitOfWork>();
            mockUoW.SetupGet(u => u.Meetings).Returns(_repository.Object);

            _controller = new MeetingsController(mockUoW.Object);
        }

        [TestMethod]
        public void Details_GivenIdDoesNotExist_ShouldReturnHttpNotFound()
        {
            var result = _controller.Details(1);

            result.Should().BeOfType<HttpNotFoundResult>();
        }

        [TestMethod]
        public void Delete_GivenIdDoesNotExist_ShouldReturnHttpNotFound()
        {
            var result = _controller.Delete(1);

            result.Should().BeOfType<HttpNotFoundResult>();
        }

        [TestMethod]
        public void Delete_GivenIdIsValid_ShouldReturnHttpStatusCodeOk()
        {
            var meeting = new Meeting() { Id = 1 };
            _repository.Setup(r => r.GetMeeting(1)).Returns(meeting);

            var result = _controller.Delete(1) as HttpStatusCodeResult;

            result.StatusCode.ShouldBeEquivalentTo((int)HttpStatusCode.OK);
        }

        [TestMethod]
        public void Edit_GetRequestGivenIdDoesNotExist_ShouldReturnHttpNotFound()
        {
            var result = _controller.Edit(1);

            result.Should().BeOfType<HttpNotFoundResult>();
        }

        [TestMethod]
        public void Edit_GetRequestGivenIdDoesExistButDateTimeIsLessThenDateTimeNow_ShouldReturnRedirectToRouteResult()
        {
            var meeting = new Meeting() { Id = 1, Date = DateTime.Now.AddDays(-1) };
            _repository.Setup(r => r.GetMeeting(1)).Returns(meeting);

            var result = _controller.Edit(1);
            
            result.Should().BeOfType<RedirectToRouteResult>();
        }

        [TestMethod]
        public void Edit_GetRequestGivenIdDoesExistButDateTimeIsLessThenDateTimeNow_ShouldRedirecTo_Meetings_Details()
        {
            var meeting = new Meeting() { Id = 1, Date = DateTime.Now.AddDays(-1) };
            _repository.Setup(r => r.GetMeeting(1)).Returns(meeting);

            var result = _controller.Edit(1) as RedirectToRouteResult;

            result.RouteValues["Action"].ShouldBeEquivalentTo("Details");
            result.RouteValues["Controller"].ShouldBeEquivalentTo("Meetings");
        }

        [TestMethod]
        public void Edit_GetResultGivenIdDoesExistDateTimeIsValid_ShouldReturnViewResult()
        {
            var meeting = new Meeting() { Id = 1, Date = DateTime.Now.AddDays(1) };
            _repository.Setup(r => r.GetMeeting(1)).Returns(meeting);

            var result = _controller.Edit(1);

            result.Should().BeOfType<ViewResult>();
        }

    }
}
