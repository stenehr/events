﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Events.Core.ViewModels;
using Events.Core.Mappers;
using FluentAssertions;

namespace Events.Tests.Mappers
{
    [TestClass]
    public class MapAttendanceTest
    {
        [TestMethod]
        public void MapIndivdualToAttendance_SetAllPropertysOfIndividualFormViewModel_ShouldReturnMeetingEntityWithAllPropertys()
        {
            var vm = new IndividualAttendanceFormViewModel()
            {
                Id = 1,
                FirstName = "A",
                LastName = "B",
                IdentityCode = "2",
                MeetingId = 1,
                PaymentTypeId = 1,
                ExtraInfo = "None"
            };
            var mapper = new MapAttendances();

            var result = mapper.MapIndividualToAttendance(vm);

            result.Id.ShouldBeEquivalentTo(1);
            result.Name.ShouldBeEquivalentTo("A B");
            result.IdentityCode.ShouldBeEquivalentTo("2");
            result.MeetingId.ShouldBeEquivalentTo(1);
            result.PaymentTypeId.ShouldBeEquivalentTo(1);
            result.Info.ShouldBeEquivalentTo("None");
            result.NumOfParticipants.ShouldBeEquivalentTo("1");
        }

        [TestMethod]
        public void MapIndividualToAttendance_SetOnlyId_ShouldReturnMeetingsEntityWithIdAndNumOfParticipants()
        {
            var vm = new IndividualAttendanceFormViewModel() { Id = 1 };
            var mapper = new MapAttendances();

            var result = mapper.MapIndividualToAttendance(vm);

            result.Id.ShouldBeEquivalentTo(1);
            result.NumOfParticipants.ShouldBeEquivalentTo("1");
        }

        //TODO: Testi mapperid
    }
}
