﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Events.Persistence.Repositories;
using System.Data.Entity;
using Events.Core.Models;
using Moq;
using Events.Persistence;
using Events.Core.ViewModels;
using Events.Core.Mappers;
using FluentAssertions;

namespace Events.Tests.Mappers
{

    [TestClass]
    public class MapMeetingsTest
    {
        [TestMethod]
        public void MapFormToMeeting_PropertysNotSet_ShouldMapTypeToMeeting()
        {
            var vm = new MeetingsFormViewModel();
            var mapper = new MapMeetings();

            Meeting result = mapper.MapFormToMeeting(vm);

            result.Should().BeOfType<Meeting>();
        }

        [TestMethod]
        public void MapFormToMeetings_DateIsNotSet_ShouldSetDateToDateTimeMinValue()
        {
            var vm = new MeetingsFormViewModel() { Id = 1, };

            var mapper = new MapMeetings();

            Meeting result = mapper.MapFormToMeeting(vm);

            result.Date.ShouldBeEquivalentTo(DateTime.MinValue);
        }

        [TestMethod]
        public void MapFormToMeetings_AllPropertysAreSet_ShouldMapAllPropertys()
        {
            var vm = new MeetingsFormViewModel()
            {
                Id = 1,
                Name = "Test",
                Date = "20.11.2017",
                Time = "20:00",
                ExtraInfo = "None",
                Location = "Tallinn"
            };

            var mapper = new MapMeetings();

            Meeting result = mapper.MapFormToMeeting(vm);

            result.Id.ShouldBeEquivalentTo(1);
            result.Name.ShouldBeEquivalentTo("Test");
            result.Date.ToString().ShouldBeEquivalentTo("11/20/2017 8:00:00 PM");
            result.ExtraInfo.ShouldBeEquivalentTo("None");
            result.Location.ShouldBeEquivalentTo("Tallinn");
        }

        [TestMethod]
        public void MapToMeetingsForm_MapEmptyMeeting_ShouldBeTypeOfMeetingsFormViewModel()
        {
            var meeting = new Meeting();
            var mapper = new MapMeetings();

            MeetingsFormViewModel result = mapper.MapToMeetingsForm(meeting);

            result.Should().BeOfType<MeetingsFormViewModel>();
        }

        [TestMethod]
        public void MapToMeetingsForm_DateTimeIsSet_ShouldReturnDateAndTimeAsString()
        {
            var meeting = new Meeting() { Date = new DateTime(2017, 11, 30, 20, 00, 00) };

            var mapper = new MapMeetings();

            MeetingsFormViewModel result = mapper.MapToMeetingsForm(meeting);
  
            result.Date.ShouldBeEquivalentTo("30.11.2017");
            result.Time.ShouldBeEquivalentTo("20:00");
        }

        [TestMethod]
        public void MapToDetailsViewModel_AllPropertysAreSet_ShouldReturnMeetingsDetailsViewModelWithAllPropertysSet()
        {
            var meeting = new Meeting()
            {
                Id = 1,
                Date = new DateTime(2017, 11, 20, 20, 00, 00),
                ExtraInfo = "None",
                Location = "Tallinn",
                Name = "Test"
            };
            var mapper = new MapMeetings();

            MeetingsDetailViewModel result = mapper.MapToDetailsViewModel(meeting);

            result.Should().BeOfType<MeetingsDetailViewModel>();
            result.Id.ShouldBeEquivalentTo(1);
            result.Date.ToString().ShouldBeEquivalentTo("11/20/2017 8:00:00 PM");
            result.ExtraInfo.ShouldBeEquivalentTo("None");
            result.Location.ShouldBeEquivalentTo("Tallinn");
            result.Name.ShouldBeEquivalentTo("Test");
        }

        [TestMethod]
        public void MapToDetailsViewModel_OnlyIdSet_ShouldReturnMeetingsDetailsViewModelWithOnlyIdSet()
        {
            var meeting = new Meeting() { Id = 1 };

            var mapper = new MapMeetings();

            MeetingsDetailViewModel result = mapper.MapToDetailsViewModel(meeting);

            result.Should().BeOfType<MeetingsDetailViewModel>();
            result.Id.ShouldBeEquivalentTo(1);
        }

        [TestMethod]
        public void MapToMainViewModel_DeclareAllPropertysOnMeeting_ShouldSetOnlyIdNameDateOnViewModel()
        {
            var meeting = new Meeting()
            {
                Id = 1,
                Date = new DateTime(2017, 11, 20, 20, 00, 00),
                ExtraInfo = "None",
                Location = "Tallinn",
                Name = "Test",
            };

            var mapper = new MapMeetings();

            var result = mapper.MapToMainViewModel(meeting);

            result.Id.ShouldBeEquivalentTo(1);
            result.Name.ShouldBeEquivalentTo("Test");
            result.Date.ShouldBeEquivalentTo("11/20/2017 8:00:00 PM");
        }
    }
}
