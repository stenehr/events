﻿
// Üksuste kustutamiseks X ikoonist
function DeleteEntry(clickedElement, msg, url, callback) {
    var that = clickedElement;
    bootbox.confirm({
        title: "Kustuta",
        message: msg + " " + that.attr("data-name") + " ?",
        buttons: {
            cancel: {
                label: "Loobu",
                className: "btn-default"
            },
            confirm: {
                label: "Jah",
                className: "btn-primary"
            },
        },
        callback: function (result) {
            if (result) {
                callback(url, clickedElement);
            }
        }
    });
}

// Ajax kustutamine, id peab olema elemendi data-id attribuudina
function ajaxDelete(url, element) {
    $.ajax({
        url: url,
        data: { id: element.attr('data-id') },
        method: "POST",
        success: function () {
            element.parents('tr').remove();
        }
    });
}

// Tänase kuupäeva saamine
function GetTodaysDate() {
    var today = new Date();
    var year = today.getFullYear();
    var month = today.getMonth() + 1;
    var day = today.getDate();

    return day + "/" + month + "/" + year;
}

$('.datepicker').datepicker({
    format: 'dd.mm.yyyy',
    language: 'et',
    orientation: 'bottom',
    autoclose: true,
    startDate: GetTodaysDate(),
});