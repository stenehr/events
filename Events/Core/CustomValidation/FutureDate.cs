﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Events.Core.CustomValidation
{
    public class FutureDate : ValidationAttribute
    {
        /// <summary>
        /// Kontroll, kas on tegemist reaalse kuupäevaga ja kas see on tulevikus
        /// </summary>
        public override bool IsValid(object value)
        {
            DateTime dateTime;
            var isValid = DateTime.TryParseExact(Convert.ToString(value),
                "dd.MM.yyyy",
                CultureInfo.CurrentCulture,
                DateTimeStyles.None,
                out dateTime);

            return (isValid && dateTime >= DateTime.Today);
        }
    }
}