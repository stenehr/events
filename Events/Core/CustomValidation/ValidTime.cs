﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Events.Core.CustomValidation
{
    public class ValidTime : ValidationAttribute
    {
        /// <summary>
        /// Kontroll, kas on reaalne kellaaeg 24h formaadis
        /// </summary>
        public override bool IsValid(object value)
        {
            DateTime time;
            var isValid = DateTime.TryParseExact(Convert.ToString(value).Replace('.', ':'),
                "HH:mm",
                CultureInfo.CurrentCulture,
                DateTimeStyles.None,
                out time);

            return (isValid);
        }
    }
}