﻿using System.Collections.Generic;
using Events.Core.Models;

namespace Events.Core.Repositories
{
    public interface IPaymentTypeRepository
    {
        IEnumerable<PaymentType> GetPaymentTypes();
    }
}