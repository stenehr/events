﻿using System.Collections.Generic;
using Events.Core.Models;

namespace Events.Core.Repositories
{
    public interface IMeetingRepository
    {
        IEnumerable<Meeting> GetAllMeetings();
        Meeting GetMeeting(int id);
        IEnumerable<Meeting> GetupcomingMeetings();
        IEnumerable<Meeting> GetPastMeetings();
        void AddMeeting(Meeting meeting);
        void RemoveMeeting(Meeting meeting);
    }
}