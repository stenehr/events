﻿using Events.Core.Models;
using System.Collections.Generic;

namespace Events.Core.Repositories
{
    public interface IAttendaceRepository
    {
        Attendance GetAttendanceByMeetingIdAttendeeCode(int meetingId, string attendeeIdentiyCode);
        IEnumerable<Attendance> GetAttendanceByMeetingId(int meetingId);
        Attendance GetAttendance(int id);
        void AddAttendance(Attendance attendance);
        void RemoveAttendance(Attendance attendee);
    }
}