﻿using Events.Core.Repositories;

namespace Events.Core
{
    public interface IUnitOfWork
    {
        IMeetingRepository Meetings { get; }
        IPaymentTypeRepository PaymentTypes { get; }
        IAttendaceRepository Attendances { get; }

        void Save();
        void Update<T>(T entity) where T : class;
    }
}