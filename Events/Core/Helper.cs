﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Events.Core
{
    public class Helper
    {
        /// <summary>
        /// Võtab täisnimest eesnime
        /// </summary>
        /// <param name="FullName">Täisnimi</param>
        /// <returns>Kõik nimed peale viimase nime</returns>
        public string GetFirstName(string FullName)
        {
            if (String.IsNullOrWhiteSpace(FullName))
                return null;

            var name = FullName.Split(' ');

            if (name.Length > 2)
            {
                var firstNames = name.Take(name.Length - 1);
                return string.Join(" ", firstNames);
            }

            return name[0];
        }

        /// <summary>
        /// Täisnimest perekonna nime saamine
        /// </summary>
        /// <param name="FullName">Täisnim</param>
        /// <returns>Täisnime viimase nime</returns>
        public string GetLastName(string FullName)
        {
            if (String.IsNullOrWhiteSpace(FullName))
                return null;

            var name = FullName.Split(' ');
            return name[name.Length - 1];
        }

        /// <summary>
        /// Parsib sisendi DateTime objektiks, kui parsimine ei õnnestu tagastab DateTime.MinValue
        /// </summary>
        /// <param name="date">kuupäev sõnendina</param>
        /// <param name="time">kellaaeg sõnendina</param>
        /// <returns>DateTime objekti</returns>
        public DateTime GetDateTime(string date, string time)
        {
            if (string.IsNullOrWhiteSpace(date) || string.IsNullOrWhiteSpace(time))
                return DateTime.MinValue;

            DateTime dateTime = DateTime.MinValue;
            time = time.Replace('.', ':');

            DateTime.TryParseExact($"{date} {time}", "dd.MM.yyyy HH:mm", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateTime);
            
            return dateTime;
        }
    }
}