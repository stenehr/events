﻿using Events.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Events.Core.ViewModels
{
    /// <summary>
    /// ViewModel ürituse osavõtjate kuvamiseks
    /// </summary>
    public class LowDetailAttendeeViewModel
    {
        public int Id { get; set; }
        public string IdentityCode { get; set; }
        public string Name { get; set; }
    }
}