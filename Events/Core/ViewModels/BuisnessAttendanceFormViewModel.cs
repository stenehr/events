﻿using Events.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Events.Core.ViewModels
{
    /// <summary>
    /// ViewModel ettevõtte lisamis/muutmis vormile
    /// </summary>
    public class BuisnessAttendanceFormViewModel
    {
        public int Id { get; set; }

        [Required, StringLength(8, MinimumLength = 8)]
        [DisplayName("Registrikood")]
        public string IdentityCode { get; set; }

        [Required, StringLength(50)]
        [DisplayName("Juriidiline nimi")]
        public string Name { get; set; }

        [Required]
        [DisplayName("Osavõtjate arv")]
        public string NumOfParticipants{ get; set; }

        [StringLength(5000)]
        [DisplayName("Lisainfo")]
        public string ExtraInfo { get; set; }

        [Required]
        [DisplayName("Makseviis")]
        public int PaymentTypeId { get; set; }

        [Required]
        public int MeetingId { get; set; }

        public string AttendeeType { get { return AttendeeTypes.Buisness; } }

        public IEnumerable<PaymentType> PaymentTypes { get; set; }

    }
}