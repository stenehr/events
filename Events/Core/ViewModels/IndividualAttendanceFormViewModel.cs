﻿using Events.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Events.Core.ViewModels
{
    /// <summary>
    /// ViewModel eraisiku lisamiseks/muutmiseks vormile
    /// </summary>
    public class IndividualAttendanceFormViewModel
    {
        public int Id { get; set; }

        [Required, StringLength(11, MinimumLength = 11)]
        [RegularExpression("\\d*", ErrorMessage = "Isikukood peab olema number")]
        [DisplayName("Isikukood")]
        public string IdentityCode { get; set; }

        [Required, StringLength(50)]
        [DisplayName("Eesnimi")]
        public string FirstName { get; set; }

        [Required, StringLength(50)]
        [DisplayName("Perekonnanimi")]
        public string LastName { get; set; }
        
        [StringLength(1000)]
        [DisplayName("Lisainfo")]
        public string ExtraInfo { get; set; }

        [Required]
        [DisplayName("Makseviis")]
        public int PaymentTypeId { get; set; }

        [Required]
        public int MeetingId { get; set; }

        public IEnumerable<PaymentType> PaymentTypes { get; set; }

        public string AttendeeType { get { return AttendeeTypes.Individual; } }

        public string FullName
        {
            get { return $"{FirstName} {LastName}"; }
        }


    }
}