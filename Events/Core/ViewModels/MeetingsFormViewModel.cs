﻿using Events.Core.CustomValidation;
using Events.Core.Models;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace Events.Core.ViewModels
{
    /// <summary>
    /// ViewModel ürituste lisamise/muutmise vormil
    /// </summary>
    public class MeetingsFormViewModel
    {
        public int Id { get; set; }

        [Required, DisplayName("Nimi")]
        public string Name { get; set; }

        [Required, DisplayName("Asukoht")]
        public string Location { get; set; }

        [FutureDate]
        [Required, DisplayName("Kuupäev")]
        public string Date { get; set; }

        [ValidTime]
        [Required, DisplayName("Kellaaeg")]
        public string Time { get; set; }

        [DisplayName("Lisainfo")]
        public string ExtraInfo { get; set; }

    }
}