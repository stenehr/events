﻿using System.Collections.Generic;

namespace Events.Core.ViewModels
{
    /// <summary>
    /// ViewModel tulevaste/möödunud ürituste kuvamiseks avalehel
    /// </summary>
    public class UpcomingPassedMeetingsViewModel
    {
        public IEnumerable<MainMeetingViewModel> UpcomingMeetings { get; set; }
        public IEnumerable<MainMeetingViewModel> PassedMeetings { get; set; }
    }
}