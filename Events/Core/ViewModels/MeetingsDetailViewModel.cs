﻿using Events.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Events.Core.ViewModels
{
    /// <summary>
    /// ViewModel ürituse info lehel kuvamiseks
    /// </summary>
    public class MeetingsDetailViewModel
    {
        public int Id { get; set; }

        [DisplayName("Ürituse Nimi")]
        public string Name { get; set; }

        [DisplayName("Toimumisaeg")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm}")]
        public DateTime Date { get; set; }

        [DisplayName("Koht")]
        public string Location { get; set; }

        [DisplayName("Lisainfo")]
        public string ExtraInfo { get; set; }

        public IEnumerable<LowDetailAttendeeViewModel> Attendees { get; set; }

        public MeetingsDetailViewModel()
        {
            Attendees = new HashSet<LowDetailAttendeeViewModel>();
        }
    }
}