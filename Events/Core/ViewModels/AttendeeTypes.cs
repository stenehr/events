﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Events.Core
{
    /// <summary>
    /// Erinevad osavõtja tüübid
    /// </summary>
    public static class AttendeeTypes
    {
        public static string Individual = "Individual";
        public static string Buisness = "Buisness";
    }
}