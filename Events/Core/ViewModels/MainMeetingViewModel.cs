﻿using Events.Core.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace Events.Core.ViewModels
{
    /// <summary>
    /// ViewModel valehel ürituste kuvamiseks
    /// </summary>
    public class MainMeetingViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
    }
}