﻿using System.ComponentModel.DataAnnotations;

namespace Events.Core.Models
{
    public class Attendance
    {
        public int Id { get; set; }

        public int MeetingId { get; set; }

        [Required, StringLength(11)]
        public string IdentityCode { get; set; }

        [Required, StringLength(100)]
        public string Name { get; set; }

        public int PaymentTypeId { get; set; }

        [Required, StringLength(1000)]
        public string NumOfParticipants { get; set; } = "1";

        [StringLength(5000)]
        public string Info { get; set; }

        [StringLength(50)]
        public string AttendeeType { get; set; }

        public virtual Meeting Meeting { get; set; }
        public virtual PaymentType PaymentType { get; set; }
    }
}