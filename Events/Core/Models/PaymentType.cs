﻿using System.ComponentModel.DataAnnotations;

namespace Events.Core.Models
{
    public class PaymentType
    {
        public int Id { get; set; }

        [Required, StringLength(30)]
        public string Type { get; set; }
    }
}