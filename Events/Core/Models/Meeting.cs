﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Events.Core.Models
{
    public class Meeting
    {
        public int Id { get; set; }

        [Required, StringLength(100)]
        public string Name { get; set; }

        [Required, StringLength(100)]
        public string Location { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [StringLength(1000)]
        public string ExtraInfo { get; set; }

        public ICollection<Attendance> Attendaces { get; set; }
    }
}