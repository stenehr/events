﻿using Events.Core.Models;
using Events.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Events.Core.Mappers
{
    public class MapAttendances
    {
        private readonly Helper _helper = new Helper();

        /// <summary>
        /// IndividualAttendanceFormViewModel vastendamine Attendance olemiks
        /// </summary>
        /// <returns>Attendance olem</returns>
        public Attendance MapIndividualToAttendance(IndividualAttendanceFormViewModel viewModel)
        {
            return new Attendance()
            {
                Id = viewModel.Id,
                IdentityCode = viewModel.IdentityCode,
                Name = viewModel.FullName,
                MeetingId = viewModel.MeetingId,
                PaymentTypeId = viewModel.PaymentTypeId,
                Info = viewModel.ExtraInfo,
                AttendeeType = viewModel.AttendeeType
            };
        }

        /// <summary>
        /// Attendance vastendamine IndividualAttendanceFormViewModel 'ks
        /// </summary>
        /// <param name="attendance">Attendance olem</param>
        /// <returns>ViewModel eraisiku vormi jaoks</returns>
        public IndividualAttendanceFormViewModel MapToIndividualFormViewModel(Attendance attendance)
        {
            return new IndividualAttendanceFormViewModel()
            {
                Id = attendance.Id,
                FirstName = _helper.GetFirstName(attendance.Name),
                LastName = _helper.GetLastName(attendance.Name),
                MeetingId = attendance.MeetingId,
                IdentityCode = attendance.IdentityCode,
                PaymentTypeId = attendance.PaymentTypeId,
                ExtraInfo = attendance.Info
            };
        }

        /// <summary>
        /// Attendance olemi vastendamine BuisnessAttendanceFormViewModel 'ks
        /// </summary>
        /// <param name="attendance">Attendance olem</param>
        /// <returns>ViewModel ettevõtte vormi jaoks</returns>
        public BuisnessAttendanceFormViewModel MapToBuisnessFormViewModel(Attendance attendance)
        {
            return new BuisnessAttendanceFormViewModel()
            {
                Id = attendance.Id,
                Name = attendance.Name,
                NumOfParticipants = attendance.NumOfParticipants,
                MeetingId = attendance.MeetingId,
                IdentityCode = attendance.IdentityCode,
                PaymentTypeId = attendance.PaymentTypeId,
                ExtraInfo = attendance.Info
            };
        }

        /// <summary>
        /// BuisnessAttendanceFormViewModel vastendamine Attendance olemiks
        /// </summary>
        /// <returns>Attendance olem</returns>
        public Attendance MapBuisnessToAttendance(BuisnessAttendanceFormViewModel viewModel)
        {
            return new Attendance()
            {
                Id = viewModel.Id,
                Name = viewModel.Name,
                IdentityCode = viewModel.IdentityCode,
                MeetingId = viewModel.MeetingId,
                PaymentTypeId = viewModel.PaymentTypeId,
                NumOfParticipants = viewModel.NumOfParticipants,
                Info = viewModel.ExtraInfo,
                AttendeeType = viewModel.AttendeeType
            };
        }

    }
}