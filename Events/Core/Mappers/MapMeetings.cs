﻿using Events.Core.Models;
using Events.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Events.Core.Mappers
{
    public class MapMeetings
    {
        private readonly Helper _helper = new Helper();

        /// <summary>
        /// Meeting olemi vastendamine MainMeetingViewModel 'ks
        /// </summary>
        /// <param name="meeting">Meeting olem</param>
        /// <returns>Meeting olem</returns>
        public MainMeetingViewModel MapToMainViewModel(Meeting meeting)
        {
            return new MainMeetingViewModel()
            {
                Id = meeting.Id,
                Name = meeting.Name,
                Date = meeting.Date
            };
        }

        /// <summary>
        /// Meeting olemi vastendamine MeetingsDetailViewModel 'ks
        /// </summary>
        /// <param name="meeting">Meeting olem</param>
        /// <returns>ViewModel ürituse info lehel kuvamiseks</returns>
        public MeetingsDetailViewModel MapToDetailsViewModel(Meeting meeting)
        {
            return new MeetingsDetailViewModel()
            {
                Id = meeting.Id,
                Name = meeting.Name,
                Date = meeting.Date,
                Location = meeting.Location,
                ExtraInfo = meeting.ExtraInfo
            };
        }

        /// <summary>
        /// MeetingsFormViewModel vastendamine Meeting olemiks
        /// </summary>
        /// <returns>Meeting olem</returns>
        public Meeting MapFormToMeeting(MeetingsFormViewModel viewModel)
        {
            return new Meeting()
            {
                Id = viewModel.Id,
                Name = viewModel.Name,
                Location = viewModel.Location,
                ExtraInfo = viewModel.ExtraInfo,
                Date = _helper.GetDateTime(viewModel.Date, viewModel.Time)
            };
        }

        /// <summary>
        /// Meeting olemi vastendamine MeetingsFormViewModel 'ks
        /// </summary>
        /// <param name="meeting">Meeting olem</param>
        /// <returns>ViewModel ürituse lisamis/muutmise vormil kuvamiseks</returns>
        public MeetingsFormViewModel MapToMeetingsForm(Meeting meeting)
        {
            return new MeetingsFormViewModel()
            {
                Id = meeting.Id,
                Name = meeting.Name,
                Date = meeting.Date.ToString("dd.MM.yyyy"),
                Time = meeting.Date.ToString("HH:mm"),
                ExtraInfo = meeting.ExtraInfo,
                Location = meeting.Location
            };
        }

  
    }
}