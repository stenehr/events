﻿using Events.Core;
using Events.Core.Mappers;
using Events.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Events.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly MapMeetings _mapper;

        public HomeController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = new MapMeetings();
        }

        public ActionResult Index()
        {
            var viewModel = new UpcomingPassedMeetingsViewModel()
            {
                UpcomingMeetings = _unitOfWork.Meetings.GetupcomingMeetings()
                    .Select(_mapper.MapToMainViewModel),
                PassedMeetings = _unitOfWork.Meetings.GetPastMeetings()
                    .Select(_mapper.MapToMainViewModel)
            };

            return View(viewModel);
        }
    }
}
