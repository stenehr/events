﻿using Events.Core;
using Events.Core.Mappers;
using Events.Core.Models;
using Events.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Events.Controllers
{
    public class AttendanceController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEnumerable<PaymentType> _paymentTypes;
        private readonly MapAttendances _mapper;

        public AttendanceController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = new MapAttendances();
            _paymentTypes = _unitOfWork.PaymentTypes.GetPaymentTypes();
        }

        public ActionResult Add(int meetingId, string attendeeType)
        {
            if (attendeeType == AttendeeTypes.Individual)
            {
                var viewModel = new IndividualAttendanceFormViewModel() { PaymentTypes = _paymentTypes, MeetingId = meetingId };
                return View("AddIndividualAttendance", viewModel);
            }
            else if (attendeeType == AttendeeTypes.Buisness)
            {
                var viewModel = new BuisnessAttendanceFormViewModel() { PaymentTypes = _paymentTypes, MeetingId = meetingId};
                return View("AddBuisnessAttendance", viewModel);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddIndividual(IndividualAttendanceFormViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return View("AddIndividualAttendance", viewModel);

            var checkAttendance = _unitOfWork.Attendances.GetAttendanceByMeetingIdAttendeeCode(viewModel.MeetingId, viewModel.IdentityCode);

            // Kui sellise isikukoodiga osavõtja on üritusele juba registreerinud - anname võimaluse andmeid muuta
            if (checkAttendance != null)
            {
                ViewBag.Error = "Olete üritusele juba registreerinud, soovi korral muutke andmeid";
                viewModel.Id = checkAttendance.Id;
                viewModel.PaymentTypes = _paymentTypes;
                return View("EditIndividualAttendance", viewModel);
            }

            var attendance = _mapper.MapIndividualToAttendance(viewModel);
            _unitOfWork.Attendances.AddAttendance(attendance);
            _unitOfWork.Save();

            return RedirectToAction("Details", "Meetings", new { Id = viewModel.MeetingId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddBuisness(BuisnessAttendanceFormViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return View("AddBuisnessAttendance", viewModel);

            var attendance = _mapper.MapBuisnessToAttendance(viewModel);
            _unitOfWork.Attendances.AddAttendance(attendance);
            _unitOfWork.Save();

            return RedirectToAction("Details", "Meetings", new { Id = viewModel.MeetingId });
        }

        public ActionResult Edit(int id)
        {
            ViewBag.Error = "";
            var attendance = _unitOfWork.Attendances.GetAttendance(id);

            if (attendance == null)
                return HttpNotFound();

            if (attendance.AttendeeType == AttendeeTypes.Individual)
            {
                var viewModel = _mapper.MapToIndividualFormViewModel(attendance);
                viewModel.PaymentTypes = _paymentTypes;
                return View("EditIndividualAttendance", viewModel);
            }
            else if (attendance.AttendeeType == AttendeeTypes.Buisness)
            {
                var viewModel = _mapper.MapToBuisnessFormViewModel(attendance);
                viewModel.PaymentTypes = _paymentTypes;
                return View("EditBuisnessAttendance", viewModel);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditIndividual(IndividualAttendanceFormViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return View("EditIndividualAttendance", viewModel);

            var attendance = _mapper.MapIndividualToAttendance(viewModel);
            _unitOfWork.Update(attendance);
            _unitOfWork.Save();

            return RedirectToAction("Details", "Meetings", new { Id = attendance.MeetingId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditBuisness(BuisnessAttendanceFormViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return View("EditBuisnessAttendance", viewModel);

            var attendance = _mapper.MapBuisnessToAttendance(viewModel);
            _unitOfWork.Update(attendance);
            _unitOfWork.Save();

            return RedirectToAction("Details", "Meetings", new { Id = attendance.MeetingId });
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var attendance = _unitOfWork.Attendances.GetAttendance(id);

            if (attendance == null)
                return HttpNotFound();

            _unitOfWork.Attendances.RemoveAttendance(attendance);
            _unitOfWork.Save();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

    }
}