﻿using Events.Core;
using Events.Core.Mappers;
using Events.Core.ViewModels;
using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Events.Controllers
{
    public class MeetingsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly MapMeetings _mapper;
        private readonly Helper _helper;

        public MeetingsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = new MapMeetings();
            _helper = new Helper();
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(MeetingsFormViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return View(viewModel);

            if (_helper.GetDateTime(viewModel.Date, viewModel.Time) <= DateTime.Now)
                return View(viewModel);

            var meeting = _mapper.MapFormToMeeting(viewModel);
            _unitOfWork.Meetings.AddMeeting(meeting);
            _unitOfWork.Save();

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Edit(int id)
        {
            var meeting = _unitOfWork.Meetings.GetMeeting(id);

            if (meeting == null)
                return HttpNotFound();

            if (meeting.Date < DateTime.Now)
                return RedirectToAction("Details", "Meetings", new { Id = id });

            var viewModel = _mapper.MapToMeetingsForm(meeting);

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MeetingsFormViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return View(viewModel);

            if (_helper.GetDateTime(viewModel.Date, viewModel.Time) < DateTime.Now)
                return RedirectToAction("Edit", "Meetings", new { Id = viewModel.Id });

            var meeting = _mapper.MapFormToMeeting(viewModel);
            _unitOfWork.Update(meeting);
            _unitOfWork.Save();

            return RedirectToAction("Details", "Meetings", new { Id = meeting.Id });
        }

        public ActionResult Details(int id)
        {
            var meeting = _unitOfWork.Meetings.GetMeeting(id);

            if (meeting == null)
                return HttpNotFound();

            var viewModel = _mapper.MapToDetailsViewModel(meeting);
            viewModel.Attendees = _unitOfWork.Attendances.GetAttendanceByMeetingId(id)
                .Select(a => new LowDetailAttendeeViewModel() { Id = a.Id, IdentityCode = a.IdentityCode, Name = a.Name });

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var meeting = _unitOfWork.Meetings.GetMeeting(id);

            if (meeting == null || meeting.Date <= DateTime.Now)
                return HttpNotFound();

            _unitOfWork.Meetings.RemoveMeeting(meeting);
            _unitOfWork.Save();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}