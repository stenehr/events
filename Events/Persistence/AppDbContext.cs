﻿using Events.Core.Models;
using System.Data.Entity;

namespace Events.Persistence
{
    public class AppDbContext : DbContext, IAppDbContext
    {
        public DbSet<Meeting> Meetings { get; set; }
        public DbSet<PaymentType> PaymentTypes { get; set; }
        public DbSet<Attendance> Attendances { get; set; }

        public AppDbContext() : base("EventsDbContext")
        {
            
        }
    }
}