﻿using Events.Core.Models;
using Events.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Events.Persistence.Repositories
{
    public class MeetingRepository : IMeetingRepository
    {
        private readonly IAppDbContext _context;

        public MeetingRepository(IAppDbContext context)
        {
            _context = context;
        }
        
        public IEnumerable<Meeting> GetAllMeetings() => _context.Meetings.ToList();

        public Meeting GetMeeting(int id) => _context.Meetings.SingleOrDefault(e => e.Id == id);

        public IEnumerable<Meeting> GetupcomingMeetings() => _context.Meetings.Where(m => m.Date >= DateTime.Now).OrderBy(m => m.Date);

        public IEnumerable<Meeting> GetPastMeetings() => _context.Meetings.Where(m => m.Date < DateTime.Now).OrderByDescending(m => m.Date);

        public void AddMeeting(Meeting meeting) => _context.Meetings.Add(meeting);

        public void RemoveMeeting(Meeting meeting) => _context.Meetings.Remove(meeting);
    }
}