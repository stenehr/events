﻿using Events.Core.Models;
using Events.Core.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Events.Persistence.Repositories
{
    public class PaymentTypeRepository : IPaymentTypeRepository
    {
        private readonly IAppDbContext _context;

        public PaymentTypeRepository(IAppDbContext context)
        {
            _context = context;
        }

        public IEnumerable<PaymentType> GetPaymentTypes() => _context.PaymentTypes;
    }
}