﻿using Events.Core.Models;
using Events.Core.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Events.Persistence.Repositories
{

    public class AttendaceRepository : IAttendaceRepository
    {
        private readonly IAppDbContext _context;

        public AttendaceRepository(IAppDbContext context)
        {
            _context = context;
        }

        public Attendance GetAttendance(int id) => _context.Attendances.SingleOrDefault(a => a.Id == id);

        public IEnumerable<Attendance> GetAttendanceByMeetingId(int meetingId) => _context.Attendances
            .Where(a => a.MeetingId == meetingId);

        public Attendance GetAttendanceByMeetingIdAttendeeCode(int meetingId, string attendeeIdentiyCode) => _context.Attendances
            .SingleOrDefault(a => a.MeetingId == meetingId && a.IdentityCode == attendeeIdentiyCode);

        public void AddAttendance(Attendance attendance) => _context.Attendances.Add(attendance);

        public void RemoveAttendance(Attendance attendee) => _context.Attendances.Remove(attendee);
    }
}