﻿using Events.Core;
using Events.Core.Repositories;
using Events.Persistence.Repositories;
using System.Data.Entity;

namespace Events.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        public IMeetingRepository Meetings { get; private set; }
        public IPaymentTypeRepository PaymentTypes { get; private set; }
        public IAttendaceRepository Attendances { get; private set; }

        public UnitOfWork(AppDbContext context)
        {
            _context = context;
            Meetings = new MeetingRepository(context);
            PaymentTypes = new PaymentTypeRepository(context);
            Attendances = new AttendaceRepository(context);
        }

        public void Save() => _context.SaveChanges();

        public void Update<T>(T entity) where T : class
        {
            _context.Entry(entity).State = EntityState.Modified;
        }

    }
}