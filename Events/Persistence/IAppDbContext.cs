﻿using System.Data.Entity;
using Events.Core.Models;

namespace Events.Persistence
{
    public interface IAppDbContext
    {
        DbSet<Attendance> Attendances { get; set; }
        DbSet<Meeting> Meetings { get; set; }
        DbSet<PaymentType> PaymentTypes { get; set; }
    }
}