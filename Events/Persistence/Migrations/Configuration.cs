namespace Events.Migrations
{
    using Events.Core.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Events.Persistence.AppDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            MigrationsDirectory = @"Persistence\Migrations";
        }

        protected override void Seed(Events.Persistence.AppDbContext context)
        {
            context.PaymentTypes.AddOrUpdate(p => p.Type,
                new PaymentType() { Id = 1, Type = "Sularaha" },
                new PaymentType() { Id = 2, Type = "Panga�lekanne" });
        }
    }
}
