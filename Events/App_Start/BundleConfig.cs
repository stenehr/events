﻿using System.Web;
using System.Web.Optimization;

namespace Events
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/libs").Include(
                        "~/Scripts/Libs/jquery-{version}.js",
                        "~/Scripts/Libs/jquery.validate*",
                        "~/Scripts/Libs/bootstrap.js",
                        "~/Scripts/Libs/bootbox.min.js",
                        "~/Scripts/Libs/bootstrap-datepicker.js",
                        "~/Scripts/Libs/locales/bootstrap-datepicker.et.min.js",
                        "~/Scripts/App/app.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-datepicker3.css",
                      "~/Content/site.css"));
        }
    }
}
